#ifndef MAIN_H_
#define MAIN_H_

#include "hal_data.h"

#define STOP 'q'
#define FORWARD_20 'w'
#define FORWARD_40 'e'
#define FORWARD_60 'r'
#define FORWARD_80 't'
#define FORWARD_100 'y'

#define BACKWARD_20 'W'
#define BACKWARD_40 'E'
#define BACKWARD_60 'R'
#define BACKWARD_80 'T'
#define BACKWARD_100 'Y'

//#define TURN_RIGHT_FW_20 'u'
//#define TURN_RIGHT_FW_40 'i'
//#define TURN_RIGHT_FW_70 'o'
//#define TURN_RIGHT_FW_80 'p'
//#define TURN_RIGHT_FW_90 'a'
//#define TURN_RIGHT_FW_100 'k'
//
//#define TURN_RIGHT_BW_20 'U' // Turn right backward
//#define TURN_RIGHT_BW_40 'I'
//#define TURN_RIGHT_BW_70 'O'
//#define TURN_RIGHT_BW_80 'P'
//#define TURN_RIGHT_BW_90 'A'
//#define TURN_RIGHT_BW_100 'K'
//
//
//#define TURN_LEFT_FW_20 's'
//#define TURN_LEFT_FW_40 'd'
//#define TURN_LEFT_FW_70 'f'
//#define TURN_LEFT_FW_80 'g'
//#define TURN_LEFT_FW_90 'h'
//#define TURN_LEFT_FW_100 'j'
//
//
//#define TURN_LEFT_BW_20 'S'
//#define TURN_LEFT_BW_40 'D'
//#define TURN_LEFT_BW_70 'F'
//#define TURN_LEFT_BW_80 'G'
//#define TURN_LEFT_BW_90 'H'
//#define TURN_LEFT_BW_100 'J'

#define TURN_RIGHT_FW_20 'u'
#define TURN_RIGHT_FW_30 'o'
#define TURN_RIGHT_FW_40 'a'
#define TURN_RIGHT_FW_60 'i'
#define TURN_RIGHT_FW_80 'k'
#define TURN_RIGHT_FW_100 'l'

#define TURN_RIGHT_BW_20 'U' // Turn right backward
#define TURN_RIGHT_BW_30 'O'
#define TURN_RIGHT_BW_40 'A'
#define TURN_RIGHT_BW_60 'I'
#define TURN_RIGHT_BW_80 'K'
#define TURN_RIGHT_BW_100 'L'

#define TURN_LEFT_FW_20 's'
#define TURN_LEFT_FW_30 'f'
#define TURN_LEFT_FW_40 'h'
#define TURN_LEFT_FW_60 'd'
#define TURN_LEFT_FW_80 'j'
#define TURN_LEFT_FW_100 'v'

#define TURN_LEFT_BW_20 'S'
#define TURN_LEFT_BW_30 'F'
#define TURN_LEFT_BW_40 'H'
#define TURN_LEFT_BW_60 'D'
#define TURN_LEFT_BW_80 'J'
#define TURN_LEFT_BW_100 'V'

#define TURN_LEFT_90_DEGREE 'z'
#define TURN_RIGHT_90_DEGREE 'Z'

//#define NORMAL_MODE 'x'
#define AUTOMATION_MODE 'X'
#define AUTOMATION_START 'c'
#define AUTOMATION_STOP  'C'

#define DEGREE_22 0.39
#define DEGREE_30 0.52
#define DEGREE_45 0.79
#define DEGREE_60 1.04

#define SPEED_MEDIUM 1
#define SPEED_LOW 0
#define SPEED_HIGH 2

#define SPEED_TURN_LOW 0
#define SPEED_TURN_HIGH 1

#define SW1 BSP_IO_PORT_01_PIN_00
#define SW2 BSP_IO_PORT_01_PIN_01
#define MODE_BUTTON1 BSP_IO_PORT_01_PIN_04
#define MODE_BUTTON2 BSP_IO_PORT_01_PIN_10

#define ABS_MIN 1400

#define axis_X_root 2048
#define axis_Y_root 2048

#define LED1 BSP_IO_PORT_01_PIN_12
#define LED2 BSP_IO_PORT_01_PIN_11

#define M0 BSP_IO_PORT_02_PIN_06
#define M1 BSP_IO_PORT_02_PIN_07

void GPIO_Init(void);
void ADC_Init(void);
uint16_t ADCx_GetValue(adc_ctrl_t *adcx_ctrl, adc_channel_t channel);
void UART_Init(void);
void UART_Send_Char(uart_ctrl_t *uartx, uint8_t char_data, volatile uint8_t *uartx_event);
void Read_Joystick(void);
uint8_t Joystick1_Result(void);

#endif
