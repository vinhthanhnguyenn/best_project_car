#include "main.h"
#include <math.h>

FSP_CPP_HEADER
void R_BSP_WarmStart(bsp_warm_start_event_t event);
FSP_CPP_FOOTER

extern volatile uart_event_t uart_event_now;
uint16_t VRx1, VRy1;
uint16_t VRx2, VRy2;

float vector_angle, vector_abs;
uint8_t vector_sector;

uint8_t remote_transmit_data, remote_transmit_data_old;
extern uint8_t speed_turn_state;
extern volatile uint8_t speed_state;
extern volatile bool turnright_90degree_flag;
extern volatile bool turnleft_90degree_flag;

extern volatile bool automation_mode;
extern volatile bool automation_flag;
extern volatile uint8_t automation_signal;

void GPIO_Init(void)
{
    R_IOPORT_PinWrite (&g_ioport_ctrl, M0, BSP_IO_LEVEL_LOW);
    R_IOPORT_PinWrite (&g_ioport_ctrl, M1, BSP_IO_LEVEL_LOW);
}

void ADC_Init(void)
{
    R_ADC_Open (&g_adc0_ctrl, &g_adc0_cfg);
    R_ADC_ScanCfg (&g_adc0_ctrl, &g_adc0_channel_cfg);

}

void Timer_Init(void)
{

    R_GPT_Open (&g_timer0_ctrl, &g_timer0_cfg);

    (void) R_GPT_Start (&g_timer0_ctrl);

}

void External_Interrupt_Init(void)
{
    R_ICU_ExternalIrqOpen (&g_external_irq1_ctrl, &g_external_irq1_cfg);
    R_ICU_ExternalIrqEnable (&g_external_irq1_ctrl);

    R_ICU_ExternalIrqOpen (&g_external_irq2_ctrl, &g_external_irq2_cfg);
    R_ICU_ExternalIrqEnable (&g_external_irq2_ctrl);

    R_ICU_ExternalIrqOpen (&g_external_irq3_ctrl, &g_external_irq3_cfg);
    R_ICU_ExternalIrqEnable (&g_external_irq3_ctrl);
}

uint16_t ADCx_GetValue(adc_ctrl_t *adcx_ctrl, adc_channel_t channel)
{
    uint16_t adc_result = 0;
    R_ADC_ScanStart (adcx_ctrl);
    adc_status_t status;
    status.state = ADC_STATE_SCAN_IN_PROGRESS;
    while (status.state == ADC_STATE_SCAN_IN_PROGRESS)
    {
        (void) R_ADC_StatusGet (adcx_ctrl, &status);
    }
    R_ADC_Read (adcx_ctrl, channel, &adc_result);
    return adc_result;
}

void UART_Init(void)
{
    fsp_err_t err = R_SCI_UART_Open (&g_uart0_ctrl, &g_uart0_cfg);
    assert(FSP_SUCCESS == err);

}

void UART_Send_Char(uart_ctrl_t *uartx, uint8_t char_data, volatile uint8_t *uartx_event)
{
    R_SCI_UART_Write (uartx, &char_data, 1);
    while (*uartx_event != UART_EVENT_TX_COMPLETE)
    {
        ;
    }

    *uartx_event = 0;
}

//void Read_Joystick(void)
//{
//    VRx1 = ADCx_GetValue (&g_adc0_ctrl, ADC_CHANNEL_0);
//
//    VRy1 = 4095 - ADCx_GetValue (&g_adc0_ctrl, ADC_CHANNEL_1);
//
//    VRx2 = 4095 - ADCx_GetValue (&g_adc0_ctrl, ADC_CHANNEL_2);
//
//    VRy2 = ADCx_GetValue (&g_adc0_ctrl, ADC_CHANNEL_11);
//
//}

void Read_Joystick(void)
{
    uint16_t VRy1_temp = 0;
    uint16_t VRx2_temp = 0;

    R_ADC_ScanStart (&g_adc0_ctrl);
    adc_status_t status;
    status.state = ADC_STATE_SCAN_IN_PROGRESS;
    while (status.state == ADC_STATE_SCAN_IN_PROGRESS)
    {
        (void) R_ADC_StatusGet (&g_adc0_ctrl, &status);
    }
    R_ADC_Read (&g_adc0_ctrl, ADC_CHANNEL_0, &VRx1);

    R_ADC_Read (&g_adc0_ctrl, ADC_CHANNEL_1, &VRy1_temp);
    VRy1 = 4095 - VRy1_temp;

    R_ADC_Read (&g_adc0_ctrl, ADC_CHANNEL_2, &VRx2_temp);
    VRx2 = 4095 - VRx2_temp;

    R_ADC_Read (&g_adc0_ctrl, ADC_CHANNEL_11, &VRy2);

}

void Vector_Result(float *angle, float *abs, uint8_t *sector)
{
    if ((VRy2 >= 2048) && (VRx2 >= 2048))
    {
        *sector = 1;
        *abs = sqrt (pow ((VRy2 - 2048), 2) + pow ((VRx2 - 2048), 2));
        *angle = atan2 ((VRy2 - 2048), (VRx2 - 2048));
    }

    else if ((VRy2 >= 2048) && (VRx2 < 2048))
    {
        *sector = 2;
        *abs = sqrt (pow ((VRy2 - 2048), 2) + pow ((2048 - VRx2), 2));
        *angle = atan2 ((VRy2 - 2048), (2048 - VRx2));
    }

    else if ((VRy2 < 2048) && (VRx2 < 2048))
    {
        *sector = 3;
        *abs = sqrt (pow ((2048 - VRy2), 2) + pow ((2048 - VRx2), 2));
        *angle = atan2 ((2048 - VRy2), (2048 - VRx2));
    }

    else if ((VRy2 < 2048) && (VRx2 >= 2048))
    {
        *sector = 4;
        *abs = sqrt (pow ((2048 - VRy2), 2) + pow ((VRx2 - 2048), 2));
        *angle = atan2 ((2048 - VRy2), (VRx2 - 2048));
    }
}

uint8_t Joystick1_Result(void)
{

//    if ((VRy1 <= (axis_Y_root + 400 * 1)) && (VRy1 >= (axis_Y_root - 400 * 1)))
//    {
//        return STOP;
//    }

    if (speed_state == SPEED_HIGH)
    {
        if ((VRy1 <= (axis_Y_root + 400 * 2)) && (VRy1 > (axis_Y_root + 400 * 1)))
        {
            return FORWARD_20;
        }

        else if ((VRy1 <= (axis_Y_root + 400 * 3)) && (VRy1 > (axis_Y_root + 400 * 2)))
        {
            return FORWARD_40;
        }

        else if ((VRy1 <= (axis_Y_root + 400 * 4)) && (VRy1 > (axis_Y_root + 400 * 3)))
        {
            return FORWARD_60;
        }

        else if ((VRy1 <= (axis_Y_root + 400 * 5)) && (VRy1 > (axis_Y_root + 400 * 4)))
        {
            return FORWARD_80;
        }

        else if (VRy1 > (axis_Y_root + 400 * 5))
        {
            return FORWARD_100;
        }
        // Back ward
        else if ((VRy1 >= (axis_Y_root - 400 * 2)) && (VRy1 < (axis_Y_root - 400 * 1)))
        {
            return BACKWARD_20;
        }

        else if ((VRy1 >= (axis_Y_root - 400 * 3)) && (VRy1 < (axis_Y_root - 400 * 2)))
        {
            return BACKWARD_40;
        }

        else if ((VRy1 >= (axis_Y_root - 400 * 4)) && (VRy1 < (axis_Y_root - 400 * 3)))
        {
            return BACKWARD_60;
        }

        else if ((VRy1 >= (axis_Y_root - 400 * 5)) && (VRy1 < (axis_Y_root - 400 * 4)))
        {
            return BACKWARD_80;
        }

        else if (VRy1 < (axis_Y_root - 400 * 5))
        {
            return BACKWARD_100;
        }
    }

    else if (speed_state == SPEED_MEDIUM)
    {

        if ((VRy1 <= (axis_Y_root + 400 * 4)) && (VRy1 > (axis_Y_root + 400 * 3)))
        {
            return FORWARD_40;
        }

        else if ((VRy1 <= (axis_Y_root + 400 * 5)) && (VRy1 > (axis_Y_root + 400 * 4)))
        {
            return FORWARD_60;
        }

        else if (VRy1 > (axis_Y_root + 400 * 5))
        {
            return FORWARD_80;
        }

        // Back ward

        else if ((VRy1 >= (axis_Y_root - 400 * 4)) && (VRy1 < (axis_Y_root - 400 * 3)))
        {
            return BACKWARD_40;
        }

        else if ((VRy1 >= (axis_Y_root - 400 * 5)) && (VRy1 < (axis_Y_root - 400 * 4)))
        {
            return BACKWARD_60;
        }

        else if (VRy1 < (axis_Y_root - 400 * 5))
        {
            return BACKWARD_80;
        }
    }

    else
    {
        if ((VRy1 <= (axis_Y_root + 400 * 4)) && (VRy1 > (axis_Y_root + 400 * 3)))
        {
            return FORWARD_20;
        }

        else if ((VRy1 <= (axis_Y_root + 400 * 5)) && (VRy1 > (axis_Y_root + 400 * 4)))
        {
            return FORWARD_40;
        }

        else if (VRy1 > (axis_Y_root + 400 * 5))
        {
            return FORWARD_60;
        }

        // Back ward

        else if ((VRy1 >= (axis_Y_root - 400 * 4)) && (VRy1 < (axis_Y_root - 400 * 3)))
        {
            return BACKWARD_20;
        }

        else if ((VRy1 >= (axis_Y_root - 400 * 5)) && (VRy1 < (axis_Y_root - 400 * 4)))
        {
            return BACKWARD_40;
        }

        else if (VRy1 < (axis_Y_root - 400 * 5))
        {
            return BACKWARD_60;
        }

    }

//    return 0;
}

uint8_t Joystick2_Result(float *angle, float *abs, uint8_t *sector)
{

    if (turnright_90degree_flag == true)
    {
        turnright_90degree_flag = false;
        return TURN_RIGHT_90_DEGREE;
    }

    else if (turnleft_90degree_flag == true)
    {
        turnleft_90degree_flag = false;
        return TURN_LEFT_90_DEGREE;
    }

    if ((VRy1 > (axis_Y_root + 400 * 1)) && (speed_turn_state == SPEED_TURN_HIGH))
    {
        if ((*sector == 1) && (*abs > ABS_MIN))
        {

            if ((*angle >= 0) && (*angle < DEGREE_22))
            {
                return TURN_RIGHT_FW_80;
            }

            else if ((*angle >= DEGREE_22) && (*angle < DEGREE_45))
            {
                return TURN_RIGHT_FW_60;
            }

            else
            {
                return Joystick1_Result ();
            }
        }

        else if ((*sector == 2) && (*abs > ABS_MIN))
        {
            if ((*angle >= 0) && (*angle < DEGREE_22))
            {
                return TURN_LEFT_FW_80;
            }

            else if ((*angle >= DEGREE_22) && (*angle < DEGREE_45))
            {
                return TURN_LEFT_FW_60;
            }

            else
            {
                return Joystick1_Result ();
            }
        }

        else if ((*sector == 3) && (*abs > ABS_MIN) && (*angle < DEGREE_30))
        {
            return TURN_LEFT_FW_60;
        }

        else if ((*sector == 3) && (*abs > ABS_MIN) && (*angle >= DEGREE_30) && (*angle < DEGREE_60))
        {
            return TURN_LEFT_FW_80;
        }

        else if ((*sector == 3) && (*abs > ABS_MIN) && (*angle >= DEGREE_60))
        {
            return TURN_LEFT_FW_100;
        }

        else if ((*sector == 4) && (*abs > ABS_MIN) && (*angle < DEGREE_30))
        {
            return TURN_RIGHT_FW_60;
        }

        else if ((*sector == 4) && (*abs > ABS_MIN) && (*angle >= DEGREE_30) && (*angle < DEGREE_60))
        {
            return TURN_RIGHT_FW_80;
        }

        else if ((*sector == 4) && (*abs > ABS_MIN) && (*angle >= DEGREE_60))
        {
            return TURN_RIGHT_FW_100;
        }

        else if (*abs <= ABS_MIN)
        {
            //return Joystick1_Result ();
            return STOP;
        }

    }

    else if ((VRy1 > (axis_Y_root + 400 * 1)) && (speed_turn_state == SPEED_TURN_LOW))
    {
        if ((*sector == 1) && (*abs > ABS_MIN))
        {

            if ((*angle >= 0) && (*angle < DEGREE_22))
            {
                return TURN_RIGHT_FW_60;
            }

            else if ((*angle >= DEGREE_22) && (*angle < DEGREE_45))
            {
                return TURN_RIGHT_FW_40;
            }

            else
            {
                return Joystick1_Result ();
            }
        }

        else if ((*sector == 2) && (*abs > ABS_MIN))
        {
            if ((*angle >= 0) && (*angle < DEGREE_22))
            {
                return TURN_LEFT_FW_60;
            }

            else if ((*angle >= DEGREE_22) && (*angle < DEGREE_45))
            {
                return TURN_LEFT_FW_40;
            }

            else
            {
                return Joystick1_Result ();
            }
        }

        else if ((*sector == 3) && (*abs > ABS_MIN) && (*angle < DEGREE_30))
        {
            return TURN_LEFT_FW_40;
        }

        else if ((*sector == 3) && (*abs > ABS_MIN) && (*angle >= DEGREE_30) && (*angle < DEGREE_60))
        {
            return TURN_LEFT_FW_60;
        }

        else if ((*sector == 3) && (*abs > ABS_MIN) && (*angle >= DEGREE_60))
        {
            return TURN_LEFT_FW_80;
        }

        else if ((*sector == 4) && (*abs > ABS_MIN) && (*angle < DEGREE_30))
        {
            return TURN_RIGHT_FW_40;
        }

        else if ((*sector == 4) && (*abs > ABS_MIN) && (*angle >= DEGREE_30) && (*angle < DEGREE_60))
        {
            return TURN_RIGHT_FW_60;
        }

        else if ((*sector == 4) && (*abs > ABS_MIN) && (*angle >= DEGREE_60))
        {
            return TURN_RIGHT_FW_80;
        }

        else if (*abs <= ABS_MIN)
        {
            //return Joystick1_Result ();
            return STOP;
        }

    }

    else if (VRy1 < (axis_Y_root - 400 * 1) && (speed_turn_state == SPEED_TURN_HIGH))
    {
        if ((*sector == 3) && (*abs > ABS_MIN))
        {
            if ((*angle >= 0) && (*angle < DEGREE_22))
            {
                return TURN_LEFT_BW_80;
            }

            else if ((*angle >= DEGREE_22) && (*angle < DEGREE_45))
            {
                return TURN_LEFT_BW_60;
            }

            else
            {
                return Joystick1_Result ();
            }
        }

        else if ((*sector == 4) && (*abs > ABS_MIN))
        {
            if ((*angle >= 0) && (*angle < DEGREE_22))
            {
                return TURN_RIGHT_BW_80;
            }

            else if ((*angle >= DEGREE_22) && (*angle < DEGREE_45))
            {
                return TURN_RIGHT_BW_60;
            }

            else
            {
                return Joystick1_Result ();
            }
        }

        else if ((*sector == 1) && (*abs > ABS_MIN) && (*angle < DEGREE_30))
        {
            return TURN_RIGHT_BW_60;
        }

        else if ((*sector == 1) && (*abs > ABS_MIN) && (*angle >= DEGREE_30) && (*angle < DEGREE_60))
        {
            return TURN_RIGHT_BW_80;
        }

        else if ((*sector == 1) && (*abs > ABS_MIN) && (*angle >= DEGREE_60))
        {
            return TURN_RIGHT_BW_100;
        }

        else if ((*sector == 2) && (*abs > ABS_MIN) && (*angle < DEGREE_30))
        {
            return TURN_LEFT_BW_60;
        }

        else if ((*sector == 2) && (*abs > ABS_MIN) && (*angle >= DEGREE_30) && (*angle < DEGREE_60))
        {
            return TURN_LEFT_BW_80;
        }

        else if ((*sector == 2) && (*abs > ABS_MIN) && (*angle >= DEGREE_60))
        {
            return TURN_LEFT_BW_100;
        }

        else if (*abs <= ABS_MIN)
        {
            //return Joystick1_Result ();
            return STOP;
        }
    }

    //***

    else if (VRy1 < (axis_Y_root - 400 * 1) && (speed_turn_state == SPEED_TURN_LOW))
    {
        if ((*sector == 3) && (*abs > ABS_MIN))
        {
            if ((*angle >= 0) && (*angle < DEGREE_22))
            {
                return TURN_LEFT_BW_60;
            }

            else if ((*angle >= DEGREE_22) && (*angle < DEGREE_45))
            {
                return TURN_LEFT_BW_40;
            }

            else
            {
                return Joystick1_Result ();
            }
        }

        else if ((*sector == 4) && (*abs > ABS_MIN))
        {
            if ((*angle >= 0) && (*angle < DEGREE_22))
            {
                return TURN_RIGHT_BW_60;
            }

            else if ((*angle >= DEGREE_22) && (*angle < DEGREE_45))
            {
                return TURN_RIGHT_BW_40;
            }

            else
            {
                return Joystick1_Result ();
            }
        }

        else if ((*sector == 1) && (*abs > ABS_MIN) && (*angle < DEGREE_30))
        {
            return TURN_RIGHT_BW_40;
        }

        else if ((*sector == 1) && (*abs > ABS_MIN) && (*angle >= DEGREE_30) && (*angle < DEGREE_60))
        {
            return TURN_RIGHT_BW_60;
        }

        else if ((*sector == 1) && (*abs > ABS_MIN) && (*angle >= DEGREE_60))
        {
            return TURN_RIGHT_BW_80;
        }

        else if ((*sector == 2) && (*abs > ABS_MIN) && (*angle < DEGREE_30))
        {
            return TURN_LEFT_BW_40;
        }

        else if ((*sector == 2) && (*abs > ABS_MIN) && (*angle >= DEGREE_30) && (*angle < DEGREE_60))
        {
            return TURN_LEFT_BW_60;
        }

        else if ((*sector == 2) && (*abs > ABS_MIN) && (*angle >= DEGREE_60))
        {
            return TURN_LEFT_BW_80;
        }

        else if (*abs <= ABS_MIN)
        {
            //return Joystick1_Result ();
            return STOP;
        }
    }

    //***

    else
    {
        return STOP;
    }

//    return 0;

}

/*******************************************************************************************************************//**
 * main() is generated by the RA Configuration editor and is used to generate threads if an RTOS is used.  This function
 * is called by main() when no RTOS is used.
 **********************************************************************************************************************/
void hal_entry(void)
{
    bsp_io_level_t pin_level;

    GPIO_Init ();

    UART_Init ();
    ADC_Init ();
    External_Interrupt_Init ();
    Timer_Init ();
    /* TODO: add your own code here */
    while (1)
    {

        if (automation_mode == false) //Normal mode
        {
            Read_Joystick ();
            Vector_Result (&vector_angle, &vector_abs, &vector_sector);

            remote_transmit_data = Joystick2_Result (&vector_angle, &vector_abs, &vector_sector);

            if (remote_transmit_data != remote_transmit_data_old)
            {
                UART_Send_Char (&g_uart0_ctrl, remote_transmit_data, &uart_event_now);
            }

            remote_transmit_data_old = remote_transmit_data;

            R_BSP_SoftwareDelay (10, BSP_DELAY_UNITS_MILLISECONDS);
        }

        else // Automation mode
        {

//            R_IOPORT_PinRead (&g_ioport_ctrl, MODE_BUTTON1, &pin_level);
//
//            if (pin_level == BSP_IO_LEVEL_LOW)
//            {
//                R_BSP_SoftwareDelay (100, BSP_DELAY_UNITS_MILLISECONDS);
//
//
//            }

            if (automation_flag == true)
            {
                automation_flag = false;
                if (automation_signal == AUTOMATION_START)
                {
                    remote_transmit_data = AUTOMATION_MODE;
                }

                else
                {
                    remote_transmit_data = STOP;
                }

            }
            if (remote_transmit_data != remote_transmit_data_old)
            {
                UART_Send_Char (&g_uart0_ctrl, remote_transmit_data, &uart_event_now);
            }

            remote_transmit_data_old = remote_transmit_data;

        }

    }

#if BSP_TZ_SECURE_BUILD
    /* Enter non-secure code */
    R_BSP_NonSecureEnter();
#endif
}

/*******************************************************************************************************************//**
 * This function is called at various points during the startup process.  This implementation uses the event that is
 * called right before main() to set up the pins.
 *
 * @param[in]  event    Where at in the start up process the code is currently at
 **********************************************************************************************************************/
void R_BSP_WarmStart(bsp_warm_start_event_t event)
{
    if (BSP_WARM_START_RESET == event)
    {
#if BSP_FEATURE_FLASH_LP_VERSION != 0

        /* Enable reading from data flash. */
        R_FACI_LP->DFLCTL = 1U;

        /* Would normally have to wait tDSTOP(6us) for data flash recovery. Placing the enable here, before clock and
         * C runtime initialization, should negate the need for a delay since the initialization will typically take more than 6us. */
#endif
    }

    if (BSP_WARM_START_POST_C == event)
    {
        /* C runtime environment and system clocks are setup. */

        /* Configure pins. */
        R_IOPORT_Open (&g_ioport_ctrl, g_ioport.p_cfg);
    }
}

#if BSP_TZ_SECURE_BUILD

BSP_CMSE_NONSECURE_ENTRY void template_nonsecure_callable ();

/* Trustzone Secure Projects require at least one nonsecure callable function in order to build (Remove this if it is not required to build). */
BSP_CMSE_NONSECURE_ENTRY void template_nonsecure_callable ()
{

}
#endif
