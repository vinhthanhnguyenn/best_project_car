#include "it.h"
volatile uart_event_t uart_event_now;
volatile uint32_t global_time = 0;
volatile uint8_t speed_state = SPEED_HIGH;
uint8_t speed_turn_state = SPEED_TURN_LOW;
volatile bool speed_flag = false;
volatile bool automation_mode = false; // Normal mode
volatile bool automation_flag = false;
volatile uint8_t automation_signal = AUTOMATION_STOP;
volatile bool turnright_90degree_flag = false;
volatile bool turnleft_90degree_flag = false;
volatile bsp_io_level_t led_state = BSP_IO_LEVEL_LOW;
volatile bsp_io_level_t led_state1 = BSP_IO_LEVEL_LOW;
volatile uint8_t flag_count = 0;
void UART_Callback(uart_callback_args_t *p_args)
{
    uart_event_now = p_args->event;
//    if(p_args->event==UART_EVENT_TX_COMPLETE)
//    {
//
//    }
}

void External_Callback1(external_irq_callback_args_t *p_args) //Joystick button 2
{
    if (automation_mode == false)
    {
        turnleft_90degree_flag = true;
    }

    else
    {
        automation_flag = true;
        automation_signal = AUTOMATION_STOP;

    }

}

void External_Callback2(external_irq_callback_args_t *p_args) // Joystick button 1
{
    if (automation_mode == false)
    {
        turnright_90degree_flag = true;
    }

    else
    {
        automation_flag = true;
        automation_signal = AUTOMATION_START;
    }

}

void External_Callback3(external_irq_callback_args_t *p_args) //Button 2
{
    bsp_io_level_t level = BSP_IO_LEVEL_LOW;

    flag_count++;

//    if (flag_count == 1)
//    {
//        speed_state = SPEED_MEDIUM;
//        automation_mode = false; // Normal mode
//
//    }

    if (flag_count == 1)
    {
        automation_mode = true; // Automation mode

    }

    if (flag_count == 2)
    {
        speed_state = SPEED_HIGH;
        automation_mode = false; // Normal mode
        flag_count = 0;

    }

//    else if (flag_count == 4)
//    {
//        speed_state = SPEED_LOW;
//        automation_mode = false; // Normal mode
//        flag_count = 0;
//
//    }

}

void Timer_Callback(timer_callback_args_t *p_args)
{
    if (TIMER_EVENT_CYCLE_END == p_args->event)
    {
        global_time++;
        if (global_time >= 100000)
        {
            global_time = 0;
        }

//        if ((speed_state == SPEED_LOW) && (global_time % 1000 == 0) && (automation_mode == false))
//        {
//
//            led_state = 1 - led_state;
//
//            R_IOPORT_PinWrite (&g_ioport_ctrl, LED2, led_state);
//
//        }
//
//        else if ((speed_state == SPEED_MEDIUM) && (global_time % 500 == 0) && (automation_mode == false))
//        {
//
//            led_state = 1 - led_state;
//
//            R_IOPORT_PinWrite (&g_ioport_ctrl, LED2, led_state);
//
//        }

        if ((speed_state == SPEED_HIGH) && (global_time % 250 == 0) && (automation_mode == false))
        {
            led_state = 1 - led_state;

            R_IOPORT_PinWrite (&g_ioport_ctrl, LED2, led_state);
            R_IOPORT_PinWrite (&g_ioport_ctrl, LED1, BSP_IO_LEVEL_HIGH);

        }

        else if ((automation_mode == true)&&(global_time % 250 == 0))
        {
            led_state1 = 1 - led_state1;

            R_IOPORT_PinWrite (&g_ioport_ctrl, LED1, led_state1);
            R_IOPORT_PinWrite (&g_ioport_ctrl, LED2, BSP_IO_LEVEL_HIGH);
        }

//        if ((global_time % 250 == 0) && (speed_turn_state == SPEED_TURN_HIGH))
//        {
//            led_state1 = 1 - led_state1;
//
//            R_IOPORT_PinWrite (&g_ioport_ctrl, LED1, led_state1);
//
//        }
//
//        else if ((global_time % 500 == 0) && (speed_turn_state == SPEED_TURN_LOW))
//        {
//            led_state1 = 1 - led_state1;
//
//            R_IOPORT_PinWrite (&g_ioport_ctrl, LED1, led_state1);
//
//        }

    }
}

