#include "hal_data.h"
#include "main.h"

FSP_CPP_HEADER
void R_BSP_WarmStart(bsp_warm_start_event_t event);
FSP_CPP_FOOTER

timer_info_t info;
uint32_t current_period_counts;

motor_t motor1, motor2, motor3, motor4;

extern volatile uint8_t remote_data;
extern volatile uint32_t automation_period;
extern volatile uint8_t automation_flag;
//uint8_t count_ss2 = 0;
//uint8_t count_ss3 = 0;

extern volatile uint8_t ss2_level;
extern volatile uint8_t ss1_level;
extern volatile uint8_t ss3_level;

extern volatile bool automation_run;

void Gpio_init(void)
{

    R_ICU_ExternalIrqOpen (&g_external_irq2_ctrl, &g_external_irq2_cfg);
    R_ICU_ExternalIrqEnable (&g_external_irq2_ctrl);

    R_ICU_ExternalIrqOpen (&g_external_irq3_ctrl, &g_external_irq3_cfg);
    R_ICU_ExternalIrqEnable (&g_external_irq3_ctrl);

    R_IOPORT_PinCfg (&g_ioport_ctrl, L1T, IOPORT_CFG_PORT_DIRECTION_OUTPUT);
    R_IOPORT_PinCfg (&g_ioport_ctrl, L1D, IOPORT_CFG_PORT_DIRECTION_OUTPUT);
    R_IOPORT_PinCfg (&g_ioport_ctrl, R1T, IOPORT_CFG_PORT_DIRECTION_OUTPUT);
    R_IOPORT_PinCfg (&g_ioport_ctrl, R1D, IOPORT_CFG_PORT_DIRECTION_OUTPUT);

    R_IOPORT_PinCfg (&g_ioport_ctrl, L2T, IOPORT_CFG_PORT_DIRECTION_OUTPUT);
    R_IOPORT_PinCfg (&g_ioport_ctrl, L2D, IOPORT_CFG_PORT_DIRECTION_OUTPUT);
    R_IOPORT_PinCfg (&g_ioport_ctrl, R2T, IOPORT_CFG_PORT_DIRECTION_OUTPUT);
    R_IOPORT_PinCfg (&g_ioport_ctrl, R2D, IOPORT_CFG_PORT_DIRECTION_OUTPUT);

    R_IOPORT_PinCfg (&g_ioport_ctrl, L3T, IOPORT_CFG_PORT_DIRECTION_OUTPUT);
    R_IOPORT_PinCfg (&g_ioport_ctrl, L3D, IOPORT_CFG_PORT_DIRECTION_OUTPUT);
    R_IOPORT_PinCfg (&g_ioport_ctrl, R3T, IOPORT_CFG_PORT_DIRECTION_OUTPUT);
    R_IOPORT_PinCfg (&g_ioport_ctrl, R3D, IOPORT_CFG_PORT_DIRECTION_OUTPUT);

    R_IOPORT_PinCfg (&g_ioport_ctrl, L4T, IOPORT_CFG_PORT_DIRECTION_OUTPUT);
    R_IOPORT_PinCfg (&g_ioport_ctrl, L4D, IOPORT_CFG_PORT_DIRECTION_OUTPUT);
    R_IOPORT_PinCfg (&g_ioport_ctrl, R4T, IOPORT_CFG_PORT_DIRECTION_OUTPUT);
    R_IOPORT_PinCfg (&g_ioport_ctrl, R4D, IOPORT_CFG_PORT_DIRECTION_OUTPUT);

    R_IOPORT_PinCfg (&g_ioport_ctrl, LED, IOPORT_CFG_PORT_DIRECTION_OUTPUT);

//    R_IOPORT_PinCfg (&g_ioport_ctrl, IR1, IOPORT_CFG_PORT_DIRECTION_INPUT);
//    R_IOPORT_PinCfg (&g_ioport_ctrl, IR2, IOPORT_CFG_PORT_DIRECTION_INPUT);
//    R_IOPORT_PinCfg (&g_ioport_ctrl, IR3, IOPORT_CFG_PORT_DIRECTION_INPUT);
//    R_IOPORT_PinCfg (&g_ioport_ctrl, IR4, IOPORT_CFG_PORT_DIRECTION_INPUT);

//************************************************************************************
    R_IOPORT_PinWrite (&g_ioport_ctrl, L1T, BSP_IO_LEVEL_LOW);
    R_IOPORT_PinWrite (&g_ioport_ctrl, L1D, BSP_IO_LEVEL_LOW);
    R_IOPORT_PinWrite (&g_ioport_ctrl, R1T, BSP_IO_LEVEL_LOW);
    R_IOPORT_PinWrite (&g_ioport_ctrl, R1D, BSP_IO_LEVEL_LOW);

    R_IOPORT_PinWrite (&g_ioport_ctrl, L2T, BSP_IO_LEVEL_LOW);
    R_IOPORT_PinWrite (&g_ioport_ctrl, L2D, BSP_IO_LEVEL_LOW);
    R_IOPORT_PinWrite (&g_ioport_ctrl, R2T, BSP_IO_LEVEL_LOW);
    R_IOPORT_PinWrite (&g_ioport_ctrl, R2D, BSP_IO_LEVEL_LOW);

    R_IOPORT_PinWrite (&g_ioport_ctrl, L3T, BSP_IO_LEVEL_LOW);
    R_IOPORT_PinWrite (&g_ioport_ctrl, L3D, BSP_IO_LEVEL_LOW);
    R_IOPORT_PinWrite (&g_ioport_ctrl, R3T, BSP_IO_LEVEL_LOW);
    R_IOPORT_PinWrite (&g_ioport_ctrl, R3D, BSP_IO_LEVEL_LOW);

    R_IOPORT_PinWrite (&g_ioport_ctrl, L4T, BSP_IO_LEVEL_LOW);
    R_IOPORT_PinWrite (&g_ioport_ctrl, L4D, BSP_IO_LEVEL_LOW);
    R_IOPORT_PinWrite (&g_ioport_ctrl, R4T, BSP_IO_LEVEL_LOW);
    R_IOPORT_PinWrite (&g_ioport_ctrl, R4D, BSP_IO_LEVEL_LOW);

    R_IOPORT_PinWrite (&g_ioport_ctrl, LED, BSP_IO_LEVEL_LOW);
    R_IOPORT_PinWrite (&g_ioport_ctrl, M0, BSP_IO_LEVEL_LOW);
    R_IOPORT_PinWrite (&g_ioport_ctrl, M1, BSP_IO_LEVEL_LOW);
}

void UART_Init(void)
{
    fsp_err_t err = R_SCI_UART_Open (&g_uart0_ctrl, &g_uart0_cfg);
    assert(FSP_SUCCESS == err);

}

void Timer_Init(void)
{
    Motor_Stop (&motor1);
    Motor_Stop (&motor2);
    Motor_Stop (&motor3);
    Motor_Stop (&motor4);

    R_GPT_Open (&g_timer1_ctrl, &g_timer1_cfg);
    R_GPT_Start (&g_timer1_ctrl);

    R_GPT_Open (&g_timer0_ctrl, &g_timer0_cfg);
    R_GPT_Start (&g_timer0_ctrl);

    Set_Duty (30);

}

void Set_Duty(uint8_t duty)
{
    (void) R_GPT_InfoGet (&g_timer1_ctrl, &info);
    current_period_counts = info.period_counts;

    uint32_t duty_cycle_counts = (uint32_t) (((uint64_t) current_period_counts * duty) / 100);
    /* Set the calculated duty cycle. */
    R_GPT_DutyCycleSet (&g_timer1_ctrl, duty_cycle_counts, GPT_IO_PIN_GTIOCA);
}

void Motor_Stop(motor_t *motor)
{

    motor->motor_state = STOP;
    motor->motor_state_old = STOP;
    motor->motor_state_change = true;

}

void Motor_Forward(motor_t *motor)
{
    if ((motor->motor_state_old) != FORWARD)
    {
        Motor_Stop (motor);
        R_BSP_SoftwareDelay (2, BSP_DELAY_UNITS_MILLISECONDS);
    }
    motor->motor_state = FORWARD;
    motor->motor_state_old = FORWARD;

}

void Motor_Backward(motor_t *motor)
{
    if ((motor->motor_state_old) != BACKWARD)
    {
        Motor_Stop (motor);
        R_BSP_SoftwareDelay (2, BSP_DELAY_UNITS_MILLISECONDS);
    }

    motor->motor_state = BACKWARD;
    motor->motor_state_old = BACKWARD;

}

void Move_Forward(uint8_t duty)
{
    Set_Duty (duty);
    Motor_Forward (&motor1);
    Motor_Forward (&motor2);
    Motor_Forward (&motor3);
    Motor_Forward (&motor4);

}

void Move_Backward(uint8_t duty)
{
    Set_Duty (duty);
    Motor_Backward (&motor1);
    Motor_Backward (&motor2);
    Motor_Backward (&motor3);
    Motor_Backward (&motor4);

}

void Turn_Left_FW(uint8_t duty)
{
    Set_Duty (duty);
    Motor_Stop (&motor1);
    Motor_Backward (&motor3);

    Motor_Forward (&motor2);
    Motor_Forward (&motor4);
}

void Turn_Right_FW_2(uint8_t duty)
{
    Set_Duty (duty);
    //Motor_Stop (&motor2);
    Motor_Backward (&motor2);
    Motor_Backward (&motor4);
    Motor_Forward (&motor1);
    Motor_Forward (&motor3);
}

void Turn_Right_FW(uint8_t duty)
{
    Set_Duty (duty);
    Motor_Stop (&motor2);
    Motor_Backward (&motor4);
    Motor_Forward (&motor1);
    Motor_Forward (&motor3);
}

void Turn_Left_BW(uint8_t duty)
{
    Set_Duty (duty);
    Motor_Forward (&motor1);
    Motor_Stop (&motor3);
    Motor_Backward (&motor2);
    Motor_Backward (&motor4);
}

void Turn_Left_BW_2(uint8_t duty)
{
    Set_Duty (duty);
    Motor_Forward (&motor1);
    Motor_Forward (&motor3);
    //Motor_Stop (&motor3);
    Motor_Backward (&motor2);
    Motor_Backward (&motor4);
}

void Turn_Right_BW(uint8_t duty)
{
    Set_Duty (duty);
    Motor_Forward (&motor2);
    Motor_Stop (&motor4);
    Motor_Backward (&motor1);
    Motor_Backward (&motor3);
}

void Stop(void)
{

    Motor_Stop (&motor1);
    Motor_Stop (&motor2);
    Motor_Stop (&motor3);
    Motor_Stop (&motor4);
}

void Turn_Left_90degree(uint8_t duty)
{
    Set_Duty (duty);
    Motor_Backward (&motor2);
    Motor_Backward (&motor4);
    Motor_Forward (&motor1);
    Motor_Forward (&motor3);

    R_BSP_SoftwareDelay (250, BSP_DELAY_UNITS_MILLISECONDS);
}

void Turn_Right_90degree(uint8_t duty)
{
    Set_Duty (duty);
    Motor_Backward (&motor1);
    Motor_Backward (&motor3);
    Motor_Forward (&motor2);
    Motor_Forward (&motor4);

    R_BSP_SoftwareDelay (250, BSP_DELAY_UNITS_MILLISECONDS);
}

//void Motor_Stop_All(void)
//{
//    R_IOPORT_PinWrite (&g_ioport_ctrl, L1T, BSP_IO_LEVEL_LOW);
//    R_IOPORT_PinWrite (&g_ioport_ctrl, L1D, BSP_IO_LEVEL_LOW);
//    R_IOPORT_PinWrite (&g_ioport_ctrl, R1T, BSP_IO_LEVEL_LOW);
//    R_IOPORT_PinWrite (&g_ioport_ctrl, R1D, BSP_IO_LEVEL_LOW);
//    R_IOPORT_PinWrite (&g_ioport_ctrl, L2T, BSP_IO_LEVEL_LOW);
//    R_IOPORT_PinWrite (&g_ioport_ctrl, L2D, BSP_IO_LEVEL_LOW);
//    R_IOPORT_PinWrite (&g_ioport_ctrl, R2T, BSP_IO_LEVEL_LOW);
//    R_IOPORT_PinWrite (&g_ioport_ctrl, R2D, BSP_IO_LEVEL_LOW);
//    R_IOPORT_PinWrite (&g_ioport_ctrl, L3T, BSP_IO_LEVEL_LOW);
//    R_IOPORT_PinWrite (&g_ioport_ctrl, L3D, BSP_IO_LEVEL_LOW);
//    R_IOPORT_PinWrite (&g_ioport_ctrl, R3T, BSP_IO_LEVEL_LOW);
//    R_IOPORT_PinWrite (&g_ioport_ctrl, R3D, BSP_IO_LEVEL_LOW);
//    R_IOPORT_PinWrite (&g_ioport_ctrl, L4T, BSP_IO_LEVEL_LOW);
//    R_IOPORT_PinWrite (&g_ioport_ctrl, L4D, BSP_IO_LEVEL_LOW);
//    R_IOPORT_PinWrite (&g_ioport_ctrl, R4T, BSP_IO_LEVEL_LOW);
//    R_IOPORT_PinWrite (&g_ioport_ctrl, R4D, BSP_IO_LEVEL_LOW);
//    R_BSP_SoftwareDelay (300, BSP_DELAY_UNITS_MILLISECONDS);
//
//}

void Moving_automation(void)
{

    //       ss3    ss2
    //        1      1       Đi thẳng
    //        1      0       Rẽ phải
    //        0      1       Đi trái
    //        0      0       Đi thẳng
    if (automation_run == true)
    {
        if ((ss3_level == 1) && (ss2_level == 1))
        {
            Move_Forward (80);

        }

        else if ((ss3_level == 1) && (ss2_level == 0) && (ss1_level == 1))
        {
            //Stop ();
            //R_BSP_SoftwareDelay (50, BSP_DELAY_UNITS_MILLISECONDS);
            Move_Backward (20);
            R_BSP_SoftwareDelay (60, BSP_DELAY_UNITS_MILLISECONDS);
            Turn_Right_FW (65);
            R_BSP_SoftwareDelay (150, BSP_DELAY_UNITS_MILLISECONDS);

        }

        else if ((ss3_level == 1) && (ss2_level == 0) && (ss1_level == 0))
        {
            //Stop ();
            //R_BSP_SoftwareDelay (50, BSP_DELAY_UNITS_MILLISECONDS);

            Move_Backward (20);
            R_BSP_SoftwareDelay (500, BSP_DELAY_UNITS_MILLISECONDS);
            Turn_Right_FW (65);
            R_BSP_SoftwareDelay (500, BSP_DELAY_UNITS_MILLISECONDS);

        }

        else if ((ss3_level == 0) && (ss2_level == 1) && (ss1_level == 1))
        {
            // Stop ();
            //R_BSP_SoftwareDelay (50, BSP_DELAY_UNITS_MILLISECONDS);
            Move_Backward (20);
            R_BSP_SoftwareDelay (60, BSP_DELAY_UNITS_MILLISECONDS);
            Turn_Left_FW (65);
            R_BSP_SoftwareDelay (150, BSP_DELAY_UNITS_MILLISECONDS);

        }

        else if ((ss3_level == 0) && (ss2_level == 1) && (ss1_level == 0))
        {
            //Stop ();
            //R_BSP_SoftwareDelay (50, BSP_DELAY_UNITS_MILLISECONDS);

            Move_Backward (20);
            R_BSP_SoftwareDelay (500, BSP_DELAY_UNITS_MILLISECONDS);

            Turn_Left_FW (65);
            R_BSP_SoftwareDelay (500, BSP_DELAY_UNITS_MILLISECONDS);

        }

        else if ((ss3_level == 0) && (ss2_level == 0) && (ss1_level == 0))
        {
            Move_Forward (80);

        }

        else
        {
            Move_Forward (80);

        }

    }

//    bsp_io_level_t level_ir1;
//    bsp_io_level_t level_ir2;
//    bsp_io_level_t level_ir3;
//
//    uint8_t ghim_level_ir2 = 0;
//    uint8_t ghim_level_ir1 = 0;
//    uint8_t ghim_level_ir3 = 0;

//    while ((automation_period < AUTOMATION_PERIOD_TIME))
//    {
//        automation_flag = 1;
//        R_IOPORT_PinRead (&g_ioport_ctrl, IR2, &level_ir2);
//        R_IOPORT_PinRead (&g_ioport_ctrl, IR1, &level_ir1);
//        R_IOPORT_PinRead (&g_ioport_ctrl, IR3, &level_ir3);
//
//        if (level_ir2 == BSP_IO_LEVEL_HIGH)
//        {
//            ghim_level_ir2 = 1;
//        }
//
//        if (level_ir1 == BSP_IO_LEVEL_HIGH)
//        {
//            ghim_level_ir1 = 1;
//        }
//
//        if (level_ir3 == BSP_IO_LEVEL_HIGH)
//        {
//            ghim_level_ir3 = 1;
//        }
//
//    }
//    automation_flag = 0;
//    automation_period = 0;
    /*
     Truth table is here
     SS3 SS1 SS2

     0   0   0       Dừng
     0   0   1       Rẽ phải
     0   1   0       Đi thẳng
     0   1   1       Rẽ phải 90 độ
     1   0   0       Rẽ trái
     1   0   1       Dừng
     1   1   0       Rẽ trái 90 độ
     1   1   1       Đi thẳng
     */
//***********************************************************************************************
//       ss3    ss2
//        0      0       Đi thẳng
//        0      1       Rẽ phải
//        1      0       Đi trái
//        1      1       Đi thẳng
    /*
     if ((ghim_level_ir2 == 0) && (ghim_level_ir3 == 0))
     {
     Stop ();
     R_BSP_SoftwareDelay (50, BSP_DELAY_UNITS_MILLISECONDS);
     while ((level_ir2 == BSP_IO_LEVEL_LOW) && (level_ir3 == BSP_IO_LEVEL_LOW) && (remote_data == AUTOMATION_MODE))
     {

     Move_Forward (30);
     R_IOPORT_PinRead (&g_ioport_ctrl, IR2, &level_ir2);
     R_IOPORT_PinRead (&g_ioport_ctrl, IR3, &level_ir3);
     }

     }

     else if ((level_ir3 == BSP_IO_LEVEL_LOW) && (level_ir2 == BSP_IO_LEVEL_HIGH))
     {

     while ((level_ir2 == BSP_IO_LEVEL_HIGH) && (remote_data == AUTOMATION_MODE))
     {
     Stop ();
     R_BSP_SoftwareDelay (5, BSP_DELAY_UNITS_MILLISECONDS);

     R_IOPORT_PinRead (&g_ioport_ctrl, IR1, &level_ir1);
     if (level_ir1 == BSP_IO_LEVEL_HIGH)
     {
     Stop ();
     R_BSP_SoftwareDelay (50, BSP_DELAY_UNITS_MILLISECONDS);
     Move_Backward (20);
     R_BSP_SoftwareDelay (150, BSP_DELAY_UNITS_MILLISECONDS);
     Turn_Right_FW (75);
     R_BSP_SoftwareDelay (100, BSP_DELAY_UNITS_MILLISECONDS);
     }

     else
     {
     Move_Backward (20);
     R_BSP_SoftwareDelay (70, BSP_DELAY_UNITS_MILLISECONDS);
     Turn_Right_FW (70);
     R_BSP_SoftwareDelay (100, BSP_DELAY_UNITS_MILLISECONDS);
     }

     R_IOPORT_PinRead (&g_ioport_ctrl, IR2, &level_ir2);

     }

     }

     else if ((level_ir3 == BSP_IO_LEVEL_HIGH) && (level_ir2 == BSP_IO_LEVEL_LOW))
     {

     //R_BSP_SoftwareDelay (5, BSP_DELAY_UNITS_MILLISECONDS);
     while ((level_ir3 == BSP_IO_LEVEL_HIGH) && (remote_data == AUTOMATION_MODE))
     {
     Stop ();
     R_BSP_SoftwareDelay (5, BSP_DELAY_UNITS_MILLISECONDS);

     R_IOPORT_PinRead (&g_ioport_ctrl, IR1, &level_ir1);
     if (level_ir1 == BSP_IO_LEVEL_HIGH)
     {
     Stop ();
     R_BSP_SoftwareDelay (100, BSP_DELAY_UNITS_MILLISECONDS);
     Move_Backward (20);
     R_BSP_SoftwareDelay (200, BSP_DELAY_UNITS_MILLISECONDS);
     Turn_Left_FW (75 + 10);
     R_BSP_SoftwareDelay (100, BSP_DELAY_UNITS_MILLISECONDS);
     }
     else
     {
     Move_Backward (20);
     R_BSP_SoftwareDelay (70, BSP_DELAY_UNITS_MILLISECONDS);
     Turn_Left_FW (70 + 10);
     R_BSP_SoftwareDelay (100, BSP_DELAY_UNITS_MILLISECONDS);
     }

     R_IOPORT_PinRead (&g_ioport_ctrl, IR3, &level_ir3);
     }

     }

     else
     {
     Stop ();
     R_BSP_SoftwareDelay (50, BSP_DELAY_UNITS_MILLISECONDS);
     while ((level_ir2 == BSP_IO_LEVEL_HIGH) && (level_ir3 == BSP_IO_LEVEL_HIGH) && (remote_data == AUTOMATION_MODE))
     {
     Move_Forward (30);
     R_IOPORT_PinRead (&g_ioport_ctrl, IR2, &level_ir2);
     R_IOPORT_PinRead (&g_ioport_ctrl, IR3, &level_ir3);
     }
     }
     */

//    if ((ghim_level_ir2 == 0) && (ghim_level_ir3 == 0))
//    {
//        Move_Forward (15);
//    }
//
//    else if ((ghim_level_ir3 == 0) && (ghim_level_ir2 == 1) && (ghim_level_ir1 == 0))
//    {
////        Stop ();
////        R_BSP_SoftwareDelay (100, BSP_DELAY_UNITS_MILLISECONDS);
//        Move_Backward (15);
//        R_BSP_SoftwareDelay (50, BSP_DELAY_UNITS_MILLISECONDS);
//        Turn_Right_FW (70);
//        R_BSP_SoftwareDelay (80, BSP_DELAY_UNITS_MILLISECONDS);
//    }
//
//    else if ((ghim_level_ir3 == 0) && (ghim_level_ir2 == 1) && (ghim_level_ir1 == 1))
//    {
//        Move_Backward (15);
//        R_BSP_SoftwareDelay (150, BSP_DELAY_UNITS_MILLISECONDS);
//
//        Turn_Right_FW (75);
//        R_BSP_SoftwareDelay (220, BSP_DELAY_UNITS_MILLISECONDS);
//
//    }
//
//    else if ((ghim_level_ir3 == 1) && (ghim_level_ir2 == 0) && (ghim_level_ir1 == 0))
//    {
//        Stop ();
//      R_BSP_SoftwareDelay (100, BSP_DELAY_UNITS_MILLISECONDS);
//        Move_Backward (15);
//        R_BSP_SoftwareDelay (50, BSP_DELAY_UNITS_MILLISECONDS);
//        Turn_Left_FW (70);
//        R_BSP_SoftwareDelay (80, BSP_DELAY_UNITS_MILLISECONDS);
//    }
//
//    else if ((ghim_level_ir3 == 1) && (ghim_level_ir2 == 0) && (ghim_level_ir1 == 1))
//    {
//        Move_Backward (15);
//        R_BSP_SoftwareDelay (150 * 2, BSP_DELAY_UNITS_MILLISECONDS);
//        Turn_Left_FW (75 + 10);
//        R_BSP_SoftwareDelay (220 * 3, BSP_DELAY_UNITS_MILLISECONDS);
//
//    }
//
//    else if ((ghim_level_ir2 == 1) && (ghim_level_ir3 == 1))
//    {
//        Move_Forward (15);
//    }
}

/*******************************************************************************************************************//**
 * main() is generated by the RA Configuration editor and is used to generate threads if an RTOS is used.  This function
 * is called by main() when no RTOS is used.
 **********************************************************************************************************************/
void hal_entry(void)
{
    /* TODO: add your own code here */

    Gpio_init (); // Reset all of pin
    Timer_Init ();
    UART_Init ();
    R_BSP_SoftwareDelay (500, BSP_DELAY_UNITS_MILLISECONDS);
    R_IOPORT_PinWrite (&g_ioport_ctrl, LED, BSP_IO_LEVEL_HIGH);

    while (1)
    {

        switch (remote_data)
        {
            case AUTOMATION_MODE:
            {
                automation_run = true;
                Moving_automation ();
                break;
            }

            case TURN_LEFT_90_DEGREE:
            {
                Turn_Left_90degree (80);
                break;
            }

            case TURN_RIGHT_90_DEGREE:
            {
                Turn_Right_90degree (80);
                break;
            }

            case REMOTE_STOP:
            {
                Stop ();
                break;
            }

            case REMOTE_FORWARD_20:
            {
                Move_Forward (20);
                break;
            }

            case REMOTE_FORWARD_40:
            {
                Move_Forward (40);
                break;
            }
            case REMOTE_FORWARD_60:
            {
                Move_Forward (60);
                break;
            }

            case REMOTE_FORWARD_80:
            {
                Move_Forward (80);
                break;
            }
            case REMOTE_FORWARD_100:
            {
                Move_Forward (100);
                break;
            }

                //********************BACKWARD

            case REMOTE_BACKWARD_20:
            {
                Move_Backward (20);
                break;
            }

            case REMOTE_BACKWARD_40:
            {
                Move_Backward (40);
                break;
            }
            case REMOTE_BACKWARD_60:
            {
                Move_Backward (60);
                break;
            }

            case REMOTE_BACKWARD_80:
            {
                Move_Backward (80);
                break;
            }
            case REMOTE_BACKWARD_100:
            {
                Move_Backward (100);
                break;
            }

                //********************TURN

//            case TURN_RIGHT_FW_30:
//            {
//                Turn_Right_FW_2 (30);
//                break;
//            }
//            case TURN_RIGHT_FW_40:
//            {
//                Turn_Right_FW_2 (40);
//                break;
//            }
//
//            case TURN_RIGHT_FW_60:
//            {
//                Turn_Right_FW_2 (60);
//                break;
//            }
//
//            case TURN_RIGHT_FW_80:
//            {
//                Turn_Right_FW_2 (80);
//                break;
//            }
//
//            case TURN_RIGHT_FW_100:
//            {
//                Turn_Right_FW_2 (100);
//                break;
//            }

            case TURN_RIGHT_FW_30:
            {
                Turn_Right_FW (30);
                break;
            }
            case TURN_RIGHT_FW_40:
            {
                Turn_Right_FW (40);
                break;
            }

            case TURN_RIGHT_FW_60:
            {
                Turn_Right_FW (60);
                break;
            }

            case TURN_RIGHT_FW_80:
            {
                Turn_Right_FW (80);
                break;
            }

            case TURN_RIGHT_FW_100:
            {
                Turn_Right_FW (100);
                break;
            }
                //********************

            case TURN_RIGHT_BW_30:
            {
                Turn_Right_BW (30);
                break;
            }
            case TURN_RIGHT_BW_40:
            {
                Turn_Right_BW (40);
                break;
            }

            case TURN_RIGHT_BW_60:
            {
                Turn_Right_BW (60);
                break;
            }

            case TURN_RIGHT_BW_80:
            {
                Turn_Right_BW (80);
                break;
            }

            case TURN_RIGHT_BW_100:
            {
                Turn_Right_BW (100);
                break;
            }
                //********************

            case TURN_LEFT_FW_30:
            {
                Turn_Left_FW (30);
                break;
            }
            case TURN_LEFT_FW_40:
            {
                Turn_Left_FW (40);
                break;
            }
            case TURN_LEFT_FW_60:
            {
                Turn_Left_FW (60);
                break;
            }
            case TURN_LEFT_FW_80:
            {
                Turn_Left_FW (80);
                break;
            }

            case TURN_LEFT_FW_100:
            {
                Turn_Left_FW (100);
                break;
            }

                //********************

//            case TURN_LEFT_BW_30:
//            {
//                Turn_Left_BW_2 (30);
//                break;
//            }
//            case TURN_LEFT_BW_40:
//            {
//                Turn_Left_BW_2 (40);
//                break;
//            }
//
//            case TURN_LEFT_BW_60:
//            {
//                Turn_Left_BW_2 (60);
//                break;
//            }
//
//            case TURN_LEFT_BW_80:
//            {
//                Turn_Left_BW_2 (80);
//                break;
//            }
//
//            case TURN_LEFT_BW_100:
//            {
//                Turn_Left_BW_2 (100);
//                break;
//            }

            case TURN_LEFT_BW_30:
            {
                Turn_Left_BW (30);
                break;
            }
            case TURN_LEFT_BW_40:
            {
                Turn_Left_BW (40);
                break;
            }

            case TURN_LEFT_BW_60:
            {
                Turn_Left_BW (60);
                break;
            }

            case TURN_LEFT_BW_80:
            {
                Turn_Left_BW (80);
                break;
            }

            case TURN_LEFT_BW_100:
            {
                Turn_Left_BW (100);
                break;
            }

            default:
            {
                Stop ();
                break;
            }

        }

//        Move_Forward (70);
//        R_BSP_SoftwareDelay (1000, BSP_DELAY_UNITS_MILLISECONDS);
//        Move_Backward (70);
//        R_BSP_SoftwareDelay (2000, BSP_DELAY_UNITS_MILLISECONDS);

//        Turn_Left (70);
//        R_BSP_SoftwareDelay (1000, BSP_DELAY_UNITS_MILLISECONDS);
//        Stop ();
//        R_BSP_SoftwareDelay (2000, BSP_DELAY_UNITS_MILLISECONDS);
//
//        Move_Forward (70);
//        R_BSP_SoftwareDelay (1000, BSP_DELAY_UNITS_MILLISECONDS);
//
//        Turn_Right (70);
//        R_BSP_SoftwareDelay (1000, BSP_DELAY_UNITS_MILLISECONDS);
//        Stop ();
//        R_BSP_SoftwareDelay (2000, BSP_DELAY_UNITS_MILLISECONDS);
//
//        Rotate_180 (70);
//        R_BSP_SoftwareDelay (1800, BSP_DELAY_UNITS_MILLISECONDS);

    }

#if BSP_TZ_SECURE_BUILD
    /* Enter non-secure code */
    R_BSP_NonSecureEnter();
#endif
}

/*******************************************************************************************************************//**
 * This function is called at various points during the startup process.  This implementation uses the event that is
 * called right before main() to set up the pins.
 *
 * @param[in]  event    Where at in the start up process the code is currently at
 **********************************************************************************************************************/
void R_BSP_WarmStart(bsp_warm_start_event_t event)
{
    if (BSP_WARM_START_RESET == event)
    {
#if BSP_FEATURE_FLASH_LP_VERSION != 0

        /* Enable reading from data flash. */
        R_FACI_LP->DFLCTL = 1U;

        /* Would normally have to wait tDSTOP(6us) for data flash recovery. Placing the enable here, before clock and
         * C runtime initialization, should negate the need for a delay since the initialization will typically take more than 6us. */
#endif
    }

    if (BSP_WARM_START_POST_C == event)
    {
        /* C runtime environment and system clocks are setup. */

        /* Configure pins. */
        R_IOPORT_Open (&g_ioport_ctrl, g_ioport.p_cfg);
    }
}

#if BSP_TZ_SECURE_BUILD

BSP_CMSE_NONSECURE_ENTRY void template_nonsecure_callable ();

/* Trustzone Secure Projects require at least one nonsecure callable function in order to build (Remove this if it is not required to build). */
BSP_CMSE_NONSECURE_ENTRY void template_nonsecure_callable ()
{

}
#endif
