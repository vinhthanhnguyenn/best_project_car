#ifndef MAIN_H_
#define MAIN_H_

#include "hal_data.h"
#define L1T BSP_IO_PORT_00_PIN_02
#define L1D BSP_IO_PORT_00_PIN_01
#define R1T BSP_IO_PORT_00_PIN_00
#define R1D BSP_IO_PORT_04_PIN_02

#define L2T BSP_IO_PORT_05_PIN_00
#define L2D BSP_IO_PORT_00_PIN_15
#define R2T BSP_IO_PORT_00_PIN_14
#define R2D BSP_IO_PORT_00_PIN_13

#define L3T BSP_IO_PORT_01_PIN_01
#define L3D BSP_IO_PORT_01_PIN_02
#define R3T BSP_IO_PORT_01_PIN_03
#define R3D BSP_IO_PORT_01_PIN_04

#define L4T BSP_IO_PORT_01_PIN_12
#define L4D BSP_IO_PORT_01_PIN_11
#define R4T BSP_IO_PORT_01_PIN_10
#define R4D BSP_IO_PORT_01_PIN_09

#define LED BSP_IO_PORT_01_PIN_00
#define M0 BSP_IO_PORT_02_PIN_06
#define M1 BSP_IO_PORT_02_PIN_07

#define REMOTE_STOP 'q'
#define REMOTE_FORWARD_20 'w'
#define REMOTE_FORWARD_40 'e'
#define REMOTE_FORWARD_60 'r'
#define REMOTE_FORWARD_80 't'
#define REMOTE_FORWARD_100 'y'

#define REMOTE_BACKWARD_20 'W'
#define REMOTE_BACKWARD_40 'E'
#define REMOTE_BACKWARD_60 'R'
#define REMOTE_BACKWARD_80 'T'
#define REMOTE_BACKWARD_100 'Y'

#define TURN_RIGHT_FW_20 'u'
#define TURN_RIGHT_FW_30 'o'
#define TURN_RIGHT_FW_40 'a'
#define TURN_RIGHT_FW_60 'i'
#define TURN_RIGHT_FW_80 'k'
#define TURN_RIGHT_FW_100 'l'

#define TURN_RIGHT_BW_20 'U' // Turn right backward
#define TURN_RIGHT_BW_30 'O'
#define TURN_RIGHT_BW_40 'A'
#define TURN_RIGHT_BW_60 'I'
#define TURN_RIGHT_BW_80 'K'
#define TURN_RIGHT_BW_100 'L'

#define TURN_LEFT_FW_20 's'
#define TURN_LEFT_FW_30 'f'
#define TURN_LEFT_FW_40 'h'
#define TURN_LEFT_FW_60 'd'
#define TURN_LEFT_FW_80 'j'
#define TURN_LEFT_FW_100 'v'

#define TURN_LEFT_BW_20 'S'
#define TURN_LEFT_BW_30 'F'
#define TURN_LEFT_BW_40 'H'
#define TURN_LEFT_BW_60 'D'
#define TURN_LEFT_BW_80 'J'
#define TURN_LEFT_BW_100 'V'

#define TURN_LEFT_90_DEGREE 'z'
#define TURN_RIGHT_90_DEGREE 'Z'

#define AUTOMATION_MODE 'X'



#define axis_X_root 2048
#define axis_Y_root 2048

#define IR1 BSP_IO_PORT_04_PIN_08
#define IR2 BSP_IO_PORT_04_PIN_09
#define IR3 BSP_IO_PORT_02_PIN_12
#define IR4 BSP_IO_PORT_02_PIN_13
#define AUTOMATION_PERIOD_TIME 80 //10ms

typedef enum
{
    STOP = 0, BACKWARD, FORWARD

} motor_state_e;

typedef struct
{
    volatile motor_state_e motor_state;
    uint8_t duty_cirlce;
    volatile bool motor_state_change;
    volatile motor_state_e motor_state_old;
} motor_t;

void Gpio_init(void);
void Timer_Init(void);
void Set_Duty(uint8_t duty);
void Motor_Stop(motor_t *motor);
void Motor_Forward(motor_t *motor);
void Motor_Backward(motor_t *motor);

#endif
