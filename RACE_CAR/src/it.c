#include "hal_data.h"
#include "main.h"
#include "it.h"

volatile uart_event_t uart_event_now;
volatile uint8_t remote_data = axis_Y_root;

uint32_t global_time = 0;
volatile uint32_t automation_period = 0;
volatile uint8_t automation_flag = 0;
extern motor_t motor1, motor2, motor3, motor4;

volatile bool ss_flag = false;
volatile uint32_t automation_count = 0;

volatile uint8_t ss2_level = 1;
volatile uint8_t ss1_level = 1;
volatile uint8_t ss3_level = 1;

volatile bool automation_run = false;

void Check_motor_in_periodcallback(motor_t *motor)
{

    if (motor == &motor1)
    {
        if (motor->motor_state == FORWARD)
        {
            R_IOPORT_PinWrite (&g_ioport_ctrl, L1D, BSP_IO_LEVEL_HIGH); // Forward
            R_IOPORT_PinWrite (&g_ioport_ctrl, R1T, BSP_IO_LEVEL_HIGH);
        }

        else if (motor->motor_state == BACKWARD)
        {
            R_IOPORT_PinWrite (&g_ioport_ctrl, L1T, BSP_IO_LEVEL_HIGH); // Backward
            R_IOPORT_PinWrite (&g_ioport_ctrl, R1D, BSP_IO_LEVEL_HIGH);

        }

        else if (motor->motor_state == STOP && motor->motor_state_change == true)
        {
            R_IOPORT_PinWrite (&g_ioport_ctrl, L1T, BSP_IO_LEVEL_LOW);
            R_IOPORT_PinWrite (&g_ioport_ctrl, L1D, BSP_IO_LEVEL_LOW);
            R_IOPORT_PinWrite (&g_ioport_ctrl, R1T, BSP_IO_LEVEL_LOW);
            R_IOPORT_PinWrite (&g_ioport_ctrl, R1D, BSP_IO_LEVEL_LOW);

            motor->motor_state_change = false;
        }
    }

    else if (motor == &motor2)
    {
        if (motor->motor_state == FORWARD)
        {
            R_IOPORT_PinWrite (&g_ioport_ctrl, L2D, BSP_IO_LEVEL_HIGH);
            R_IOPORT_PinWrite (&g_ioport_ctrl, R2T, BSP_IO_LEVEL_HIGH);
        }

        else if (motor->motor_state == BACKWARD)
        {
            R_IOPORT_PinWrite (&g_ioport_ctrl, L2T, BSP_IO_LEVEL_HIGH);
            R_IOPORT_PinWrite (&g_ioport_ctrl, R2D, BSP_IO_LEVEL_HIGH);
        }

        else if (motor->motor_state == STOP && motor->motor_state_change == true)
        {
            R_IOPORT_PinWrite (&g_ioport_ctrl, L2T, BSP_IO_LEVEL_LOW);
            R_IOPORT_PinWrite (&g_ioport_ctrl, L2D, BSP_IO_LEVEL_LOW);
            R_IOPORT_PinWrite (&g_ioport_ctrl, R2T, BSP_IO_LEVEL_LOW);
            R_IOPORT_PinWrite (&g_ioport_ctrl, R2D, BSP_IO_LEVEL_LOW);
            motor->motor_state_change = false;
        }
    }

    else if (motor == &motor3)
    {
        if (motor->motor_state == FORWARD)
        {
            R_IOPORT_PinWrite (&g_ioport_ctrl, L3T, BSP_IO_LEVEL_HIGH);
            R_IOPORT_PinWrite (&g_ioport_ctrl, R3D, BSP_IO_LEVEL_HIGH);
        }

        else if (motor->motor_state == BACKWARD)
        {
            R_IOPORT_PinWrite (&g_ioport_ctrl, L3D, BSP_IO_LEVEL_HIGH);
            R_IOPORT_PinWrite (&g_ioport_ctrl, R3T, BSP_IO_LEVEL_HIGH);
        }

        else if (motor->motor_state == STOP && motor->motor_state_change == true)
        {
            R_IOPORT_PinWrite (&g_ioport_ctrl, L3T, BSP_IO_LEVEL_LOW);
            R_IOPORT_PinWrite (&g_ioport_ctrl, L3D, BSP_IO_LEVEL_LOW);
            R_IOPORT_PinWrite (&g_ioport_ctrl, R3T, BSP_IO_LEVEL_LOW);
            R_IOPORT_PinWrite (&g_ioport_ctrl, R3D, BSP_IO_LEVEL_LOW);
            motor->motor_state_change = false;
        }
    }

    else
    {
        if (motor->motor_state == FORWARD)
        {
            R_IOPORT_PinWrite (&g_ioport_ctrl, L4T, BSP_IO_LEVEL_HIGH);
            R_IOPORT_PinWrite (&g_ioport_ctrl, R4D, BSP_IO_LEVEL_HIGH);
        }

        else if (motor->motor_state == BACKWARD)
        {
            R_IOPORT_PinWrite (&g_ioport_ctrl, L4D, BSP_IO_LEVEL_HIGH);
            R_IOPORT_PinWrite (&g_ioport_ctrl, R4T, BSP_IO_LEVEL_HIGH);
        }

        else if (motor->motor_state == STOP && motor->motor_state_change == true)
        {
            R_IOPORT_PinWrite (&g_ioport_ctrl, L4T, BSP_IO_LEVEL_LOW);
            R_IOPORT_PinWrite (&g_ioport_ctrl, L4D, BSP_IO_LEVEL_LOW);
            R_IOPORT_PinWrite (&g_ioport_ctrl, R4T, BSP_IO_LEVEL_LOW);
            R_IOPORT_PinWrite (&g_ioport_ctrl, R4D, BSP_IO_LEVEL_LOW);

            motor->motor_state_change = false;
        }
    }

}

void Check_motor_in_PWMcallback(motor_t *motor)
{

    if (motor == &motor1)
    {
        if (motor->motor_state == FORWARD)
        {

            R_IOPORT_PinWrite (&g_ioport_ctrl, R1T, BSP_IO_LEVEL_LOW);
        }

        else if (motor->motor_state == BACKWARD)
        {
            R_IOPORT_PinWrite (&g_ioport_ctrl, L1T, BSP_IO_LEVEL_LOW);

        }

    }

    else if (motor == &motor2)
    {
        if (motor->motor_state == FORWARD)
        {

            R_IOPORT_PinWrite (&g_ioport_ctrl, R2T, BSP_IO_LEVEL_LOW);
        }

        else if (motor->motor_state == BACKWARD)
        {
            R_IOPORT_PinWrite (&g_ioport_ctrl, L2T, BSP_IO_LEVEL_LOW);

        }

    }

    else if (motor == &motor3)
    {
        if (motor->motor_state == FORWARD)
        {
            R_IOPORT_PinWrite (&g_ioport_ctrl, L3T, BSP_IO_LEVEL_LOW);

        }

        else if (motor->motor_state == BACKWARD)
        {

            R_IOPORT_PinWrite (&g_ioport_ctrl, R3T, BSP_IO_LEVEL_LOW);
        }

    }

    else
    {
        if (motor->motor_state == FORWARD)
        {
            R_IOPORT_PinWrite (&g_ioport_ctrl, L4T, BSP_IO_LEVEL_LOW);

        }

        else if (motor->motor_state == BACKWARD)
        {

            R_IOPORT_PinWrite (&g_ioport_ctrl, R4T, BSP_IO_LEVEL_LOW);
        }

    }

}

void Period_Callback(timer_callback_args_t *p_args)
{
    if (p_args->event == TIMER_EVENT_CYCLE_END)
    {
        //*********************************************
        global_time++;
        if (automation_flag == 1)
        {
            automation_period++;
        }

        if (global_time >= 5000)
        {
            global_time = 0;
            R_IOPORT_PinWrite (&g_ioport_ctrl, LED, BSP_IO_LEVEL_HIGH);
        }
        //*********************************************

        if (ss_flag == true)
        {
            automation_count++;
        }

        if (automation_count >= AUTOMATION_PERIOD_TIME)
        {
            automation_count = 0;
            ss_flag = false;

            automation_run = true;

        }

        Check_motor_in_periodcallback (&motor1);
        Check_motor_in_periodcallback (&motor2);
        Check_motor_in_periodcallback (&motor3);
        Check_motor_in_periodcallback (&motor4);
    }
}

void PWM_Callback(timer_callback_args_t *p_args)
{
    if (p_args->event == TIMER_EVENT_CAPTURE_A)
    {

        Check_motor_in_PWMcallback (&motor1);
        Check_motor_in_PWMcallback (&motor2);
        Check_motor_in_PWMcallback (&motor3);
        Check_motor_in_PWMcallback (&motor4);

    }
}

void UART_Callback(uart_callback_args_t *p_args)
{
    uart_event_now = p_args->event;

    if (p_args->event == UART_EVENT_RX_CHAR)
    {
        remote_data = (uint8_t) p_args->data;
        R_IOPORT_PinWrite (&g_ioport_ctrl, LED, BSP_IO_LEVEL_LOW);

    }
}

void External_Callback2(external_irq_callback_args_t *p_args)
{
    bsp_io_level_t level;
    fsp_err_t err;

    if (remote_data == AUTOMATION_MODE)
    {
        automation_run = false;
        ss_flag = true;

        err = R_IOPORT_PinRead (&g_ioport_ctrl, IR2, &level);
        assert(FSP_SUCCESS == err);
        if (level == BSP_IO_LEVEL_LOW)
        {
            ss2_level = 0;
        }

        else
        {
            ss2_level = 1;
        }

    }

}

void External_Callback1(external_irq_callback_args_t *p_args)
{

    bsp_io_level_t level;
    fsp_err_t err;

    if (remote_data == AUTOMATION_MODE)
    {
        automation_run = false;
        //ss_flag = true;

        err = R_IOPORT_PinRead (&g_ioport_ctrl, IR1, &level);
        assert(FSP_SUCCESS == err);
        if (level == BSP_IO_LEVEL_LOW)
        {
            ss1_level = 0;
        }

        else
        {
            ss1_level = 1;
        }
    }

}

void External_Callback3(external_irq_callback_args_t *p_args)
{

    bsp_io_level_t level;
    fsp_err_t err;

    if (remote_data == AUTOMATION_MODE)
    {
        automation_run = false;
        ss_flag = true;

        err = R_IOPORT_PinRead (&g_ioport_ctrl, IR3, &level);
        assert(FSP_SUCCESS == err);
        if (level == BSP_IO_LEVEL_LOW)
        {
            ss3_level = 0;
        }

        else
        {
            ss3_level = 1;
        }
    }

}

